<?php

namespace Magenest\Movie\Block;

use Magento\Framework\View\Element\Template;
use Magento\Backend\Block\Template\Context;
use Magenest\Movie\Model\ResourceModel\Director\CollectionFactory as DirectorCollectionFactory;
use Magenest\Movie\Model\ResourceModel\Actor\CollectionFactory as ActorCollectionFactory;
use Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory as MovieCollectionFactory;
use Magenest\Movie\Model\ResourceModel\MovieActor\CollectionFactory as MovieActorCollectionFactory;

class Showdata extends Template
{
    public $directorCollection;
    public $actorCollection;
    public $movieCollection;
    // public $movieActorCollection;

    public function __construct(Context $context, DirectorCollectionFactory $directorCollection,
    ActorCollectionFactory $actorCollection, MovieCollectionFactory $movieCollection, array $data = [])
    {
        $this->directorCollection = $directorCollection;
        $this->actorCollection = $actorCollection;
        $this->movieCollection = $movieCollection;
        // $this->movieActorCollection = $movieActorCollection;
        parent::__construct($context, $data);
    }

    public function getDirectorCollection()
    {
        return $this->directorCollection->create();
    }
    public function getActorCollection()
    {
        return $this->actorCollection->create();
    }
    public function getMovieCollection()
    {
        $director = $this->directorCollection->create();
        $movie = $this->movieCollection->create();
        $directorTable = $director->getTable('magenest_director');

        $movie->getSelect()->join(['director' => $directorTable], "main_table.director_id = director.director_id", ['director_name' => 'director.name']);

        return $movie;

    }
    // public function countMovieRow()
    // {
    //     $connection = $this->getConnection();
    //     $select = $connection->select();
    //     $select->from($this->getTable('magenest_movie'), 'COUNT(*)');
    //     $result = (int)$connection->fetchOne($select);
    //     return $result;
    // }
}
