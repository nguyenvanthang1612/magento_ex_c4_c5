<?php

namespace Magenest\Movie\Block;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Helper\SecureHtmlRenderer;

class DefaultValueField extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $connection;

    public function __construct(
        Context $context,
        ResourceConnection $connection,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ) {
        parent::__construct($context, $data);
        $this->connection = $connection;
    }
    public function render(AbstractElement $element)
    {
        $this->_eventManager->dispatch("magenest_movie_default_value", ['data' => $element]);
        return parent::render($element);
    }
}
