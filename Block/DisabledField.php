<?php

namespace Magenest\Movie\Block;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\View\Helper\SecureHtmlRenderer;

class DisabledField extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $connection;

    public function __construct(
        Context $context,
        ResourceConnection $connection,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ) {
        parent::__construct($context, $data);
        $this->connection = $connection;
    }
    public function render(AbstractElement $element)
    {
        $element->setReadonly(true, true)->setValue($this->countMovieRow());
        return parent::render($element);
    }

    public function countMovieRow()
    {
        $connection = $this->connection->getConnection();
        $table = $connection->getTableName('magenest_movie');
        $select = $this->connection->getConnection()->select();
        // $select->from($this->getTable('magenest_movie'), 'COUNT(*)');
        $select->from($this->connection->getTableName('magenest_movie'), 'COUNT(*)');
        $result = (int)$connection->fetchOne($select);
        return $result;
    }

}
