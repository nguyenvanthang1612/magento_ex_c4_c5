<?php

namespace Magenest\Movie\Block\Cms;

use Magento\Framework\View\Element\Template;

class CustomerGroupName extends Template
{
    protected $_customerSession;
    protected $_customerGroupCollection;
    protected $groupRepository;

    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Group $customerGroupCollection,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        array $data = []
    ){
        $this->_customerSession = $customerSession;
        $this->_customerGroupCollection = $customerGroupCollection;
        $this->groupRepository = $groupRepository;
        parent::__construct($context, $data);
    }

    public function getGroupNameById()
    {
        $currentGroupId = $this->_customerSession->getCustomer()->getGroupId(); //Get customer group Id , you have already this so directly get name
        $collection = $this->_customerGroupCollection->load($currentGroupId);
        return $collection->getCustomerGroupCode();//Get group name
//        $group = $this->groupRepository->getById($currentGroupId);
//        return $group->getCode();
    }

}
