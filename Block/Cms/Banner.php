<?php

namespace Magenest\Movie\Block\Cms;

use Magento\Framework\View\Element\Template;

class Banner extends Template
{
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function getTitle(): \Magento\Framework\Phrase
    {
        return __('HelloWorld!');
    }
}
