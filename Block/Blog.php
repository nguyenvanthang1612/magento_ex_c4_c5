<?php

namespace Magenest\Movie\Block;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Backend\Block\Template\Context;
use Magenest\Movie\Model\ResourceModel\Blog\CollectionFactory;


class Blog extends Template
{
    /**
     * @var CollectionFactory
     */
    public $blogCollection;
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;
    /**
     * @var \Magenest\Movie\Model\BlogFactory
     */
    protected $blogFactory;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var \Magenest\Movie\Model\ResourceModel\Blog
     */
    protected $blogResource;


    /**
     * Blog constructor.
     * @param Context $context
     * @param CollectionFactory $blogCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magenest\Movie\Model\BlogFactory $blogFactory,
        UrlInterface $urlBuilder,
        Context $context,
        CollectionFactory $blogCollection,
        \Magenest\Movie\Model\ResourceModel\Blog $blogResource,

        array $data = []
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->blogFactory = $blogFactory;
        $this->urlBuilder = $urlBuilder;
        $this->blogCollection = $blogCollection;
        $this->blogResource = $blogResource;

        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getBlogCollection()
    {
        return $this->blogCollection->create();
    }

    public function getUrlKey($id)
    {
        $blog = $this->blogFactory->create();
        $this->blogResource->load($blog, $id);
        return $blog->getUrlKey();
    }
}
