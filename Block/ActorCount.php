<?php

namespace Magenest\Movie\Block;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\View\Helper\SecureHtmlRenderer;

class ActorCount extends \Magento\Config\Block\System\Config\Form\Field  
{ 
    protected $connection;

    public function __construct(
        Context $context,
        ResourceConnection $connection,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ) {
        parent::__construct($context, $data);
        $this->connection = $connection;
    }

    public function render(AbstractElement $element)
    {
        $element->setReadonly(true, true)->setValue($this->countActorRow());
        return parent::render($element);
    }

    public function countActorRow()
    {
        $connection = $this->connection->getConnection();
        $table = $connection->getTableName('magenest_actor');
        $select = $this->connection->getConnection()->select();
        $select->from($this->connection->getTableName('magenest_actor'), 'COUNT(*)');
        $result = (int)$connection->fetchOne($select);
        return $result;
    }

}