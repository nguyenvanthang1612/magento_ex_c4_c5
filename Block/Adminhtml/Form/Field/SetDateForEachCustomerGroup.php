<?php

namespace Magenest\Movie\Block\Adminhtml\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Directory\Model\Country;
use Magento\Framework\DataObject;
use Magento\Framework\Registry;
//use Magento\Sales\Ui\Component\Listing\Column\CustomerGroup;
use Magenest\Movie\Block\Adminhtml\Form\Field\CustomerGroupColumn;

class SetDateForEachCustomerGroup extends AbstractFieldArray
{
    private $countryRenderer;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    protected function _prepareToRender()
    {

        $this->addColumn(
            'select_date',
            [
                'label' => __('Date'),
                'id' => 'select_date',
                'class' => 'daterecuring',
                'style' => 'width:200px'
            ]
        );

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')->toOptionArray();

        $this->addColumn(
            'date_title',
            [
                'label' => __('Customer Group'),
                'class' => 'required-entry',
                'style' => 'width:300px',
                'renderer' => $this->getCountryRenderer(),
//                'extra_params' => 'multiple="multiple"'

            ]
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = __('MoreAdd');
    }

    protected function _prepareArrayRow(DataObject $row): void
    {
        $options = [];

        $countries = $row->getCustomerGroupName()??[];
        if (count($countries) > 0) {
            foreach ($countries as $country) {
                $options['option_' . $this->getCountryRenderer()->calcOptionHash($country)]
                    = 'selected="selected"';
            }
        }

        $row->setData('option_extra_attrs', $options);
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = parent::_getElementHtml($element);

        $script = '<script type="text/javascript">
                require(["jquery", "jquery/ui", "mage/calendar"], function (jq) {
                    jq(function(){
                        function bindDatePicker() {
                            setTimeout(function() {
                                jq(".daterecuring").datepicker( { dateFormat: "mm/dd/yy" } );
                            }, 50);
                        }
                        bindDatePicker();
                        jq("button.action-add").on("click", function(e) {
                            bindDatePicker();
                        });
                    });
                });
            </script>';
        $html .= $script;
        return $html;
    }

    private function getCountryRenderer()
    {
        $this->countryRenderer = $this->getLayout()->createBlock(
            \Magenest\Movie\Block\Adminhtml\Form\Field\CountryColumn::class,
            '',
            ['data' => ['is_render_to_js_template' => true]]
        );
        return $this->countryRenderer;
    }

}
