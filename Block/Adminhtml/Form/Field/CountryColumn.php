<?php
declare(strict_types=1);
namespace Magenest\Movie\Block\Adminhtml\Form\Field;

//use PayPal\Braintree\Helper\Country;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

class CountryColumn extends Select
{
    protected $_customerSession;
    protected $_customerGroupCollection;
//    private $countryHelper;

    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $customerGroupCollection,
//        Country $countryHelper,
        array $data = []
    ) {
        $this->_customerSession = $customerSession;
        $this->_customerGroupCollection = $customerGroupCollection;
        parent::__construct($context, $data);
//        $this->countryHelper = $countryHelper;
    }

    public function setInputName($value)
    {
        return $this->setName($value . '[]');
    }

    public function _toHtml(): string
    {
        $currentGroupId = $this->_customerSession->getCustomer()->getGroupId();
        $collection = $this->_customerGroupCollection->create();

        if (!$this->getOptions()) {
            $this->setOptions((array)$collection->toOptionArray());
        }
//        $this->setExtraParams('multiple="multiple"');
        return parent::_toHtml();
    }

}
