<?php

namespace Magenest\Movie\Block\Adminhtml;

use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory as InvoiceCollectionFactory;
use \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory as CreditmemoCollectionFactory;

class Page extends Template
{
    /**
     * @var \Magento\Framework\Module\FullModuleList
     */
    private $fullModuleList;
    /**
     * @var CustomerCollectionFactory
     */
    protected $CustomerCollectionFactory;
    /**
     * @var ProductCollectionFactory
     */
    protected $ProductCollectionFactory;
    /**
     * @var OrderCollectionFactory
     */
    protected $OrderCollectionFactory;
    /**
     * @var InvoiceCollectionFactory
     */
    protected $InvoiceCollectionFactory;
    /**
     * @var CreditmemoCollectionFactory
     */
    protected $CreditmemoCollectionFactory;
    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customers;

    /**
     * Page constructor.
     * @param Template\Context $context
     * @param \Magento\Framework\Module\FullModuleList $fullModuleList
     * @param CustomerCollectionFactory $CustomerCollectionFactory
     * @param ProductCollectionFactory $ProductCollectionFactory
     * @param OrderCollectionFactory $OrderCollectionFactory
     * @param InvoiceCollectionFactory $InvoiceCollectionFactory
     * @param CreditmemoCollectionFactory $CreditmemoCollectionFactory
     * @param \Magento\Customer\Model\Customer $customers
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Framework\Module\FullModuleList $fullModuleList,
        CustomerCollectionFactory $CustomerCollectionFactory,
        ProductCollectionFactory $ProductCollectionFactory,
        OrderCollectionFactory $OrderCollectionFactory,
        InvoiceCollectionFactory $InvoiceCollectionFactory,
        CreditmemoCollectionFactory $CreditmemoCollectionFactory,
        \Magento\Customer\Model\Customer $customers,
        array $data = []
    ) {
        $this->CustomerCollectionFactory = $CustomerCollectionFactory;
        $this->ProductCollectionFactory = $ProductCollectionFactory;
        $this->OrderCollectionFactory = $OrderCollectionFactory;
        $this->InvoiceCollectionFactory = $InvoiceCollectionFactory;
        $this->CreditmemoCollectionFactory = $CreditmemoCollectionFactory;
        $this->customers = $customers;
        $this->fullModuleList = $fullModuleList;
        parent::__construct($context, $data);
    }

    public function countAllModule()
    {
        return count($this->fullModuleList->getAll());
    }

    public function countCustomer() {
        return count($this->customers->getCollection()
            ->addAttributeToSelect('*')
            ->load());
    }

    public function countProduct() {
        $collection = $this->ProductCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        return count($collection);
    }

    /**
     * @return int|void
     */
    public function countOrder() {
        $collection = $this->OrderCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        return count($collection);
    }

    public function countInvoice() {
        $collection = $this->InvoiceCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        return count($collection);
    }

    public function countCreditmemo() {
        $collection = $this->CreditmemoCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        return count($collection);
    }

    public function countVendorModule()
    {
        $all = $this->fullModuleList->getNames();
        $count = 0;
        foreach ($all as $name) {
            $cut = explode('_', $name);
            if ($cut[0] !== "Magento") {
                $count += 1;
            }
        }
        return $count;
    }
}
