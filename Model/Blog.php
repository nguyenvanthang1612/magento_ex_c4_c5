<?php

namespace Magenest\Movie\Model;

class Blog extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'magenest_blog';
    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\ResourceModel\Blog');
    }
}
