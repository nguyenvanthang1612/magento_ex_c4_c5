<?php

namespace Magenest\Movie\Model\ResourceModel\Blog;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\Blog', 'Magenest\Movie\Model\ResourceModel\Blog');
    }

}
