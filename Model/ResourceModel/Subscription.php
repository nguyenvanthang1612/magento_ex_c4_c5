<?php

namespace Magenest\Movie\Model\ResourceModel;

class Subscription extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    public function _construct() {
        $this->_init('magenest_director', 'director_id');
        $this->_init('magenest_movie', 'movie_id');
        $this->_init('magenest_actor', 'actor_id');
        $this->_init('magenest_movie_actor', 'actor_id', 'movie_id');
    }
}