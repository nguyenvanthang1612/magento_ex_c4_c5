<?php

namespace Magenest\Movie\Model\Source;

//use Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory as MovieCollectionFactory;
use Magenest\Movie\Model\ResourceModel\Director\CollectionFactory as DirectorCollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

class DirectoryId implements OptionSourceInterface
{
    /**
     * @var CollectionFactory
     */
//    protected $MovieCollectionFactory;
    protected $DirectorCollectionFactory;

    public function __construct(
//        MovieCollectionFactory $MovieCollectionFactory,
        DirectorCollectionFactory $DirectorCollectionFactory
    ) {
//        $this->MovieCollectionFactory = $MovieCollectionFactory;
        $this->DirectorCollectionFactory = $DirectorCollectionFactory;
    }

    public function toOptionArray()
    {
        $director = $this->DirectorCollectionFactory->create();
        $options[] = ['label' => '-- Please Select --', 'value' => ''];

        $director->getSelect('*');
        foreach ($director as $category) {
            $options[] = [
                'label' => $category->getName(),
                'value' => $category->getDirectorId(),
            ];
        }

        return $options;
    }
}
