<?php

namespace Magenest\Movie\Model\Source;

use  Magento\User\Model\ResourceModel\User\CollectionFactory as UserCollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

class AuthorId implements OptionSourceInterface
{
    /**
     * @var CollectionFactory
     */
    protected $UserCollectionFactory;

    public function __construct(
        UserCollectionFactory $UserCollectionFactory
    ) {
        $this->UserCollectionFactory = $UserCollectionFactory;
    }

    public function toOptionArray()
    {
        $users = $this->UserCollectionFactory->create();
        $options[] = ['label' => '-- Please Select --', 'value' => ''];

        $users->getSelect('*');
        foreach ($users as $user) {
            $options[] = [
                'label' => $user->getUserName(),
                'value' => $user->getUserId(),
            ];
        }

        return $options;
    }
}

