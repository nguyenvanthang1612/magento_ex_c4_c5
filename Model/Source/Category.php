<?php

namespace Magenest\Movie\Model\Source;

class Category implements \Magento\Framework\Option\ArrayInterface
{ 
    //Below function is supposed to return options.
    public function toOptionArray()
    {
        return [ 
            ['value' => 1, 'label' => 'show'], 
            ['value' => 2, 'label' => 'not show'] 
        ];
    }
}