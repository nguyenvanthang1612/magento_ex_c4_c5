<?php

namespace Magenest\Movie\Model\Plugin\Checkout;

class LayoutProcessor
{
    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        $children = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']['children'];
        // custom input select
        $children['custom_select'] = array_merge($children, [
            'component' => 'Magento_Ui/js/form/element/select',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes.custom_select',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
                'options' => [
                    ['value' => '1', 'label' => __('Miền Bắc')],
                    ['value' => '2', 'label' => __('Miền Trung')],
                    ['value' => '3', 'label' => __('Miền Nam')]
                ],
            ],
            'dataScope' => 'shippingAddress.custom_attributes.custom_select',
            'provider' => 'checkoutProvider',
            'label' => 'vn region',
            'visible' => true,
            'validation' => [],
            'sortOrder' => 270,
            'id' => 'vn_region'
        ]);

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']['shipping-address-fieldset']['children'] = $children;
        return $jsLayout;
    }
}
