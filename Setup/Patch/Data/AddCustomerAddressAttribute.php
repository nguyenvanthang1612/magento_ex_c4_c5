<?php
namespace Magenest\Movie\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddCustomerAddressAttribute implements DataPatchInterface
{
    /**
     * @var CustomerSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    private $setup;

    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * AccountPurposeCustomerAttribute constructor.
     * @param ModuleDataSetupInterface $setup
     * @param Config $eavConfig
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $setup,
        Config $eavConfig,
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->setup = $setup;
        $this->eavConfig = $eavConfig;
    }

    /** We'll add our customer attribute here
     * @throws \Exception
     */
    public function apply()
    {
        $customerSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);

        $customerSetup->addAttribute('customer_address', 'vn_region', [
            'type' => 'int',
            'input' => 'select',
            'label' => 'vn_region',
            'required' => false,
            'default' => 0,
            'visible' => true,
            'user_defined' => true,
            'source' => \Magenest\Movie\Model\Config\Source\Options::class,
            'visible_on_front' => true,
            'system' => false,
            'is_visible_in_grid' => true,
            'is_used_in_grid' => true,
            'is_filterable_in_grid' => true,
            'is_searchable_in_grid' => true,
            'position' => 90,
            'option' => ['values' => []]
        ]);
        $newAttribute = $this->eavConfig->getAttribute('customer_address', 'vn_region');
        $newAttribute->addData([
            'used_in_forms' => [
                'adminhtml_customer_address',
                'adminhtml_customer',
                'customer_address_edit',
                'customer_register_address',
                'customer_address',
            ],
        ]);
        $newAttribute->save();
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }
}
