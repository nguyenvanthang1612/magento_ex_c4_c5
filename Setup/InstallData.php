<?php

namespace Magenest\Movie\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    protected $_directorFactory;
    protected $_movieFactory;
    protected $_actorFactory;
    protected $_movieActorFactory;

    public function __construct(
        \Magenest\Movie\Model\DirectorFactory $directorFactory,
        \Magenest\Movie\Model\MovieFactory $movieFactory,
        \Magenest\Movie\Model\ActorFactory $actorFactory,
        \Magenest\Movie\Model\MovieActorFactory $movieActorFactory
    ) {
        $this->_directorFactory = $directorFactory;
        $this->_movieFactory = $movieFactory;
        $this->_actorFactory = $actorFactory;
        $this->_movieActorFactory = $movieActorFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $dataDirector = [
            'name' => "Magento 2"
        ];
        $director = $this->_directorFactory->create();
        $director->addData($dataDirector)->save();

        $dataMovie = [
            'name' => "zombie",
            'description' => "how to do zombie",
            'rating' => 8,
            'director_id' => 1
        ];
        $movie = $this->_movieFactory->create();
        $movie->addData($dataMovie)->save();

        $dataActor = [
            'name' => "Van Thang"
        ];
        $actor = $this->_actorFactory->create();
        $actor->addData($dataActor)->save();

        $dataMovieActor = [
            'movie_id' => 1,
            "actor_id" => 1,
        ];
        $movieActor = $this->_movieActorFactory->create();
        $movieActor->addData($dataMovieActor)->save();
    }
}
