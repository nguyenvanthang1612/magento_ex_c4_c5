<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\Event\Observer;

class ChangeValue implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Logger $logger
     */
    public function __construct(
        WriterInterface $configWriter,
        Logger $logger
    ) {
        $this->logger = $logger;
        $configWriter->save('magenest/moviepage/text_field', 'Pong');
    }

    public function execute(Observer $observer)
    {
        $data = $observer->getData('data');
        $data->setData('value', 'Pong');
    }
}
