<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\Event\Observer;

class AutoFlush implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    private \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool;
    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    private \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList;

    /**
     * AutoFlush constructor.
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     */
    public function __construct(
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
    ) {
        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
    }

    /**
     * @param Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $types = array('full_page');
        foreach ($types as $type) {
            $this->cacheTypeList->cleanType($type);
        }
        foreach ($this->cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}
