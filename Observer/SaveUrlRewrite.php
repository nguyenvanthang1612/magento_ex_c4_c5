<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\Event\Observer;

class SaveUrlRewrite implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\UrlRewrite\Model\UrlRewriteFactory
     */
    protected $urlRewriteFactory;
    /**
     * @var \Magento\UrlRewrite\Model\ResourceModel\UrlRewrite
     */
    protected $urlRewriteResource;


    public function __construct(
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewrite $urlRewriteResource,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory
    ){
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->urlRewriteResource = $urlRewriteResource;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function execute(Observer $observer)
    {
        $data = $observer->getObject()->getId();

        $requestPath = $observer->getObject()->getUrlRewrite();

        $dataUrlRewrite = [
            'entity_type' =>  'blog',
            'entity_id' =>  0,
            'request_path' => $requestPath,
            'target_path' => 'movie/blog/detail/id/' . $data,
            'store_id' => 1
        ];
        $url = $this->urlRewriteFactory->create();
        $this->urlRewriteResource->load($url, 'movie/blog/detail/id/' . $data ,'target_path');

        $dataUrlRewrite = [
            'entity_type' =>  'blog',
            'entity_id' =>  0,
            'request_path' => $observer->getObject()->getUrlRewrite(),
            'target_path' => 'movie/blog/detail/id/' . $data,
            'store_id' => 1
        ];

        $url = $this->urlRewriteFactory->create();
        $url->addData($dataUrlRewrite);
        $this->urlRewriteResource->save($url);

    }
}
