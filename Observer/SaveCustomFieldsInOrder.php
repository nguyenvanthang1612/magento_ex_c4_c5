<?php
namespace Magenest\Movie\Observer;

class SaveCustomFieldsInOrder implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $order = $observer->getEvent()->getOrder();
        $address = $observer->getEvent()->getAddress();

        $order->setData('custom_select', $address->getCustomFieldText());

        return $this;
    }
}
