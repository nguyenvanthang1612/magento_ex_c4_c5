<?php

namespace Magenest\Movie\Plugin\Minicart;

use Magento\Framework\Exception\NoSuchEntityException;

class Image
{
    protected $imageHelper;
    protected $productFactory;
    protected $_productRepository;

    public function __construct(
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        $this->imageHelper = $imageHelper;
        $this->_productRepository = $productRepository;
    }
    public function afterGetItemData($subject, $result, $item)
    {
        $product = $item->getProduct();
        if ($product->getTypeId() == 'configurable') {
            $childSku = $item->getData('sku');
            try {
                $product = $this->_productRepository->get($childSku);
            } catch (NoSuchEntityException $e) {
            }
        }
        $name = $product->getName();
        $url = $this->imageHelper->init($product, 'product_thumbnail_image')->getUrl();
        $result['product_image']['src'] = $url;
        $result['product_name'] = $name;

        return $result;
    }

}
