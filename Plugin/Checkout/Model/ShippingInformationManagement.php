<?php
namespace Magenest\Movie\Plugin\Checkout\Model;

use Magento\Quote\Model\QuoteRepository;
use Magento\Customer\Api\AddressRepositoryInterface;

class ShippingInformationManagement
{
    protected $addressRepository;

    public function __construct(AddressRepositoryInterface $addressRepository) {
        $this->addressRepository = $addressRepository;
    }

    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {

        if(!$extAttributes = $addressInformation->getExtensionAttributes())
        {
            return;
        }

        $address = $this->addressRepository->getActive($cartId);

        $address->setCustomFieldText($extAttributes->getCustomFieldText());
    }
}
