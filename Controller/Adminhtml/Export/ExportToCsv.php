<?php

namespace Magenest\Movie\Controller\Adminhtml\Export;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Ui\Controller\Adminhtml\Export\GridToCsv;
use Magento\Ui\Model\Export\ConvertToCsv;
use Psr\Log\LoggerInterface;
use Magento\Framework\Stdlib\DateTime;

class ExportToCsv extends GridToCsv implements HttpGetActionInterface
{
    protected $_collectionFactory;
    protected $_fileFactory;
    private \Magento\Framework\Filesystem\Directory\WriteInterface $directory;
    protected DateTime $dateTime;

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */

    public function __construct(
        Context $context,
        ConvertToCsv $converter,
        FileFactory $fileFactory,
        \Magento\Framework\Filesystem $filesystem,
        Filter $filter = null,
        LoggerInterface $logger = null,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        DateTime $dateTime
    )
    {
        $this->dateTime = $dateTime;
        $this->_collectionFactory = $collectionFactory;
        $this->converter = $converter;
        $this->_fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context, $converter, $fileFactory, $filter, $logger);
    }

    public function getItemCollection()
    {
        $collection = $this->_collectionFactory->create();
        $collection->getSelect()
            ->join(['so' =>'sales_order_item'],'main_table.entity_id = so.order_id');
        return $collection;
    }

    public function execute()
    {
        $name = date('m-d-Y-H-i-s');
        $filepath = 'export/export-data-' .$name. '.csv'; // at Directory path Create a Folder Export and FIle
        $this->directory->create('export');

        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();

        $columns = [
            'increment_id',
            'store_name',
            'created_at',
            'product_name',
            'qty',
            'price',
            'grand total',
            'subtotal',
            'tax amount',
            'discount amount',
            'row total'
        ];

        foreach ($columns as $column)
        {
            $header[] = $column; //storecolumn in Header array
        }

        $stream->writeCsv($header);

        $location_collection = $this->getItemCollection();

        foreach($location_collection as $item){

            $itemData = [];

            // column name must same as in your Database Table
            $itemData[] = $item->getData('increment_id');
            $itemData[] = $item->getData('store_name');
            $itemData[] = $this->dateTime->gmDate("D M j G:i:s T Y", strtotime($item->getData('created_at')));
            $itemData[] = $item->getData('name');
            $itemData[] = $item->getData('qty_ordered');
            $itemData[] = $item->getData('price');
            $itemData[] = $item->getData('grand_total');
            $itemData[] = $item->getData('subtotal');
            $itemData[] = $item->getData('tax_amount');
            $itemData[] = $item->getData('discount_amount');
            $itemData[] = $item->getData('row_total');

            $stream->writeCsv($itemData);

        }
        $stream->unlock();
        $stream->close();
        $content = [
            'type' => 'filename',
            'value' => $filepath,
            'rm' => true  // can delete file after use
        ];

        $csvfilename = 'locator-import-'.$name.'.csv';

        return $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);
    }
}
