<?php

namespace Magenest\Movie\Controller\Adminhtml\Blog;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magenest\Movie\Model\Blog as Model;

class InlineEdit extends Action
{
<<<<<<< HEAD
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var Model
     */
    protected $model;

    /**
     * InlineEdit constructor.
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param Model $model
     */
=======
    protected $jsonFactory;
    protected $model;

>>>>>>> 2ef3b1bf14507074167a3723d0862734ae31343e
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Model $model
    )
    {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->model = $model;
    }

<<<<<<< HEAD
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
=======
>>>>>>> 2ef3b1bf14507074167a3723d0862734ae31343e
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];
        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (empty($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $entityId) {
                    $modelData = $this->model->load($entityId);
                    try {
                        $modelData->setData(array_merge($modelData->getData(), $postItems[$entityId]));
                        $modelData->save();
                    } catch (\Exception $e) {
                        $messages[] = "[Error:]  {$e->getMessage()}";
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}
