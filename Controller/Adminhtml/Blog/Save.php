<?php

namespace Magenest\Movie\Controller\Adminhtml\Blog;

use function Assert\lazy;

Class Save extends \Magento\Backend\App\Action
{
<<<<<<< HEAD
    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;
    /**
     * @var \Magenest\Movie\Model\ResourceModel\Blog
     */
    protected $blogResource;
    /**
     * @var \Magenest\Movie\Model\BlogFactory
     */
    protected $blogFactory;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
=======
    protected $dataPersistor;
    protected $blogResource;
    protected $blogFactory;
    protected $resourceConnection;
>>>>>>> 2ef3b1bf14507074167a3723d0862734ae31343e
    protected $messageManager;
    /*protected $urlRewriteFactory;
    protected $urlRewriteResource;*/

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magenest\Movie\Model\ResourceModel\Blog $blogResource,
<<<<<<< HEAD
=======
        \Magento\UrlRewrite\Model\ResourceModel\UrlRewrite $urlRewriteResource,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory,
>>>>>>> 2ef3b1bf14507074167a3723d0862734ae31343e
        \Magenest\Movie\Model\BlogFactory $blogFactory,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
<<<<<<< HEAD
=======
        $this->urlRewriteResource = $urlRewriteResource;
>>>>>>> 2ef3b1bf14507074167a3723d0862734ae31343e
        $this->messageManager = $messageManager;
        $this->resourceConnection = $resourceConnection;
        $this->blogResource = $blogResource;
        $this->blogFactory = $blogFactory;
        $this->dataPersistor = $dataPersistor;
<<<<<<< HEAD
        parent::__construct($context);
    }


    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
=======
        $this->urlRewriteFactory = $urlRewriteFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */

>>>>>>> 2ef3b1bf14507074167a3723d0862734ae31343e
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $dataBlog = [
            'title' =>  $this->getRequest()->getParam('title'),
            'description' => $this->getRequest()->getParam('description'),
            'content' => $this->getRequest()->getParam('content'),
            'url_rewrite' => $this->getRequest()->getParam('url_rewrite'),
            'status' => $this->getRequest()->getParam('status'),
            'author_id' => $this->getRequest()->getParam('author_id')
        ];

<<<<<<< HEAD
=======
        $dataUrlRewrite = [
            'entity_type' =>  'blog',
            'entity_id' =>  0,
            'request_path' => $this->getRequest()->getParam('url_rewrite'),
            'target_path' => 'movie/blog/detail/id/',
            'store_id' => 1
        ];
>>>>>>> 2ef3b1bf14507074167a3723d0862734ae31343e

        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('magenest_blog');
        $select = $connection->select()->from(array('vi' => $tableName), array('url_rewrite'));
        $urlOld = $connection->fetchAll($select);
        $urlNew = $this->getRequest()->getParam('url_rewrite');

        foreach ($urlOld as $eachUrlOld) {
            if ($urlNew == $eachUrlOld['url_rewrite'])
            {
                $this->messageManager->addError(__("Error"));
                return $resultRedirect->setPath('movie/blog/form');
            }
        }

        $blog = $this->blogFactory->create();
        $blog->addData($dataBlog);
        $this->blogResource->save($blog);
<<<<<<< HEAD
//        $this->_eventManager->dispatch("magenest_blog_auto_flush_after_save");

=======

        $url = $this->urlRewriteFactory->create();
        $url->addData($dataUrlRewrite);
        $this->urlRewriteResource->save($url);
>>>>>>> 2ef3b1bf14507074167a3723d0862734ae31343e
        return $resultRedirect->setPath('movie/blog/index');
    }
}
