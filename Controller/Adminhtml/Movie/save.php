<?php

namespace Magenest\Movie\Controller\Adminhtml\Movie;

Class  save extends \Magento\Backend\App\Action
{
    protected $dataPersistor;
    protected $_movieFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magenest\Movie\Model\MovieFactory $movieFactory,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->_movieFactory = $movieFactory;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $dataMovie = [
            'name' => $data['name'],
            'description' => $data['description'],
            'rating' => $data['rating'],
            'director_id' => $data['director_id']
        ];
        $movie = $this->_movieFactory->create();
        $movie->addData($dataMovie);
        $this->_eventManager->dispatch("magenest_movie_before_save", ['movieData' => $movie]);
        $movie->save();
        return $resultRedirect->setPath('movie/movie/index');
    }
}
