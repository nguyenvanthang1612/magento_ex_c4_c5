<?php

namespace Magenest\Movie\Ui\Component\Listing\Column;

use Magenest\Movie\Model\ResourceModel\Director\CollectionFactory as DirectorCollectionFactory;
use Magenest\Movie\Model\ResourceModel\Movie\CollectionFactory as MovieCollectionFactory;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class DirectorColumn extends Column
{
    public $directorCollection;
    public $movieCollection;

    // public $movieActorCollection;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        DirectorCollectionFactory $directorCollection,
        MovieCollectionFactory $movieCollection,
        array $components = [],
        array $data = [])
    {
        $this->directorCollection = $directorCollection;
        $this->movieCollection = $movieCollection;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     * @throws NoSuchEntityException
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {

            }
        }
        return parent::prepareDataSource($dataSource);
    }
}
