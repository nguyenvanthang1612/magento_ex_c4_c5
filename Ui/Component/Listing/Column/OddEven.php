<?php

namespace   Magenest\Movie\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class OddEven extends Column
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     * @throws NoSuchEntityException
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if ($item['entity_id'] % 2 == 0) {
                    $item['magenest_movie_column'] = '<span class="grid-severity-critical">ERROR</span>';
                } else {
                    $item['magenest_movie_column'] = '<span class="grid-severity-notice">SUCCESS</span>';
                }
            }
        }
        return parent::prepareDataSource($dataSource);
    }
}
