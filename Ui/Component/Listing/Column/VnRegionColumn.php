<?php

namespace Magenest\Movie\Ui\Component\Listing\Column;

use Magento\Customer\Model\ResourceModel\Address\CollectionFactory;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class VnRegionColumn extends Column
{
    public $addressCollection;

    // public $movieActorCollection;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CollectionFactory $addressCollection,
        array $components = [],
        array $data = []
    ){
        $this->addressCollection = $addressCollection;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
//                $item[$this->getData('custom_select')] = $this->getData('custom_select');
//                $item[$this->getData('vn_region')] = $this->getData('vn_region');
            }
        }
        return parent::prepareDataSource($dataSource);
    }
}
